package com.mercury.test;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.common.io.Files;



public class AutoIt {

	public static void main(String[] args) throws InterruptedException, IOException {
		
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://imgbb.com/");
		Thread.sleep(3000);
		WebElement btnPhotoUpload = driver.findElement(By.xpath("//a[@class='btn btn-big blue']"));
		btnPhotoUpload.click();
		Thread.sleep(3000);
		Runtime.getRuntime().exec("./AutoIT/FileUpload.exe");
		Thread.sleep(10000);
		WebElement uploadPhoto = driver.findElement(By.xpath("//button[@class='btn btn-big green']"));
		uploadPhoto.click();
		Thread.sleep(3000);
		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		Files.copy(src, new File("./screenshot/photo.png"));
		Thread.sleep(3000);
		driver.close();

	}

}
