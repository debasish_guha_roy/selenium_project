package com.mercury.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;

public class MR {

	WebDriver driver;

	// ===============================================================================

	public void browserAppLaunch() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
	}

	// ===============================================================================

	/*
	 * Simple login process
	 */
	public void login() throws IOException, InterruptedException {
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");

		WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));
		login.click();

		Thread.sleep(3000);
	}

	// ===============================================================================

	/*
	 * Login with screenshot
	 */
	public void loginWithScreenShot() throws IOException, InterruptedException {
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");

		WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));
		login.click();

		Thread.sleep(4000);

		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("./screenshots/snap.png"));

		Thread.sleep(3000);
	}

	// ================================================================================

	/*
	 * Login with Sikuli
	 */
	public void loginSikuli() throws FindFailed {

		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");

		Screen screen = new Screen();
		Pattern btnLogin = new Pattern("./sikulifiles/btnlogin.PNG");
		screen.click(btnLogin);
	}

	// ================================================================================

	/*
	 * Login with Robot
	 */
	public void loginRobot() throws AWTException {
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");

		Robot r1 = new Robot();
		r1.keyPress(KeyEvent.VK_ENTER);
	}

	// =================================================================================

	/*
	 * Login with Action
	 */
	public void loginAction() {
		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");

		WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));

		Actions builder = new Actions(driver);

		builder.moveToElement(login).build().perform();

		builder.click(login).build().perform();
	}

	// ================================================================================

	/*
	 * Photo Upload with AutoIT
	 */
	public void autoITTest() throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://imgbb.com/");

		Thread.sleep(4000);

		WebElement btnStartUpload = driver.findElement(By.xpath("//a[@class='btn btn-big blue']"));
		btnStartUpload.click();

		Thread.sleep(2000);

		Runtime.getRuntime().exec("./autoitexe/autoitfileupload.exe");

		Thread.sleep(6000);

		WebElement btnUpload = driver.findElement(By.xpath("//button[@class='btn btn-big green']"));
		btnUpload.click();

		Thread.sleep(5000);
	}

	// ================================================================================

	/*
	 * Login with ImplicitWait
	 */
	public void loginImplicitWait() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));
		login.click();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	// ================================================================================

	/*
	 * Login with Explicit Wait
	 */
	public void loginExplicite() {
		new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='userName']")));

		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys("dasd");

		new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='password']")));

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys("dasd");

		new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Login']")));

		WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));
		login.click();
	}

	// ==================================================================================

	/*
	 * Login with Property File Data
	 */
	public void loginWithPropertyFile() throws IOException, InterruptedException {
		File file = new File("./testdata/testdata.properties");
		FileInputStream fileInput = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fileInput);

		Thread.sleep(3000);

		WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
		uName.sendKeys(prop.getProperty("Username"));

		Thread.sleep(2000);

		WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
		pwd.sendKeys(prop.getProperty("Password"));

		Thread.sleep(2000);

		WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));
		login.click();

	}

	// =================================================================================

	/*
	 * Data Driven Login testing with Excel
	 */
	public void loginDataDrivenExcelSheet() throws BiffException, IOException, InterruptedException {
		File src = new File("./testdata/exceldata.xls");
		Workbook wb = Workbook.getWorkbook(src);
		Sheet sh1 = wb.getSheet("login");

		int rows = sh1.getRows();

		for (int i = 1; i < rows; i++) {
			String userNameDataFromExcel = sh1.getCell(0, i).getContents();
			String passwordDataFromExcel = sh1.getCell(1, i).getContents();

			Thread.sleep(2000);

			WebElement uName = driver.findElement(By.xpath("//input[@name='userName']"));
			uName.sendKeys(userNameDataFromExcel);
			Thread.sleep(2000);

			WebElement pwd = driver.findElement(By.xpath("//input[@name='password']"));
			pwd.sendKeys(passwordDataFromExcel);
			Thread.sleep(2000);

			WebElement login = driver.findElement(By.xpath("//input[@value='Login']"));
			login.click();
			Thread.sleep(5000);

			WebElement home = driver.findElement(By.xpath("//a[contains(text(),'Home')]"));
			home.click();
			Thread.sleep(5000);

		}
	}

	// =====================================================================================

	/*
	 * Scroll down handling
	 */
	public void scrollDown() throws InterruptedException
	{
            driver = new ChromeDriver();
            driver.manage().window().fullscreen();
            driver.get("http://toolsqa.com/iframe-practice-page/");
            Thread.sleep(6000);         
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.scrollBy(0,500)");
            Thread.sleep(9000);                         
	}
	
	//======================================================================================

	/*
	 * Iframe Handling
	 */
	public void frameHandling() throws InterruptedException {
		 	driver = new ChromeDriver();
		 	driver.manage().window().fullscreen();
        	driver.get("http://demo.guru99.com/test/guru99home/");
        	Thread.sleep(6000);         
        	JavascriptExecutor js = (JavascriptExecutor) driver;
        	js.executeScript("window.scrollBy(0,2300)");
        	Thread.sleep(6000);         
        	driver.switchTo().frame("a077aa5e");
        	Thread.sleep(4000);         
        	WebElement linkbtn = driver.findElement(By.xpath("//img[@src='Jmeter720.png']"));
        	linkbtn.click();
	}
	
	//=======================================================================================

	
	/*
	 * Web Table handling mechanism
	 */
	public void webTableHandling() {

	}


//AutoIt application

public void AutoIt() throws InterruptedException, IOException

{
	
	
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://imgbb.com/");
	Thread.sleep(3000);
	WebElement btnPhotoUpload = driver.findElement(By.xpath("//a[@class='btn btn-big blue']"));
	btnPhotoUpload.click();
	Thread.sleep(3000);
	Runtime.getRuntime().exec("./AutoIT/FileUpload.exe");
	Thread.sleep(10000);
	WebElement uploadPhoto = driver.findElement(By.xpath("//button[@class='btn btn-big green']"));
	uploadPhoto.click();
	Thread.sleep(3000);
	File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	Files.copy(src, new File("./screenshot/photo.png"));
	Thread.sleep(3000);
	driver.close();

}

public void AutoItWindows() throws InterruptedException, IOException

{

	
	Runtime.getRuntime().exec("./AutoIT/Shutdown.exe");
	Thread.sleep(10000);
	
}
}
